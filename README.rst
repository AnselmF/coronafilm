===========================
Corona-Filme mit matplotlib
===========================

Dies ist ein Python-Skript, das auf Kreisebene aufgelöst aus
RKI-Coronadaten Filme von Inzidenzen und Altersscores macht.

Der Code hilft bestimmt auch, andere nach BRD-Kreisen aufgelöste Daten
mit cartopy zu plotten, auch wenn ich zugebe, dass ein paar mehr
Erklärungen im Text der Bastelbarkeit guttäten.  Ich liefere die auf
Bestelltung nach.

Ein paar Blog-Posts zum Hintergrund:

* `Blog-Post zum Parsing der RKI-Daten`_
* `Corona als Film, Inzidenzen`_
* `Corona als Film Alters-Scores`_

.. _Corona als Film Alters-Scores: https://blog.tfiu.de/corona-film-teil-2.html
.. _Corona als Film, Inzidenzen: https://blog.tfiu.de/corona-als-film.html
.. _Blog-Post zum Parsing der RKI-Daten: https://blog.tfiu.de/falscher-instinkt.html

Um die Filme zu reproduzieren:

#) python, matplotlib und cartopy installieren
#) Dieses Repository clonen
#) ``mkdir data; cd data``
#) Aus
  https://github.com/robert-koch-institut/SARS-CoV-2_Infektionen_in_Deutschland.git
  das neueste csv runterladen; der Name ist nicht mehr gut vorhersehbar.
  Clone dieses Repo *nicht* (es ist riesig).
#) Von 
  https://gdz.bkg.bund.de/index.php/default/open-data/gebietseinheiten-1-2-500-000-ge2500.html
  vg2500_01-01.utm32s.shape.zip besorgen und entpacken, aus dem
  resultierenden Verzeichnis vg2500/*_krs* ins aktuelle Verzeichnis
  kopieren
#) ``cd ..; mkmovie.py -i 3`` (oder so; probiert ``mkmovie.py -h`` für
  ein paar Optionen, die beim Basteln helfen)
#) Das dauert eine ganze Weile und schreibt eine Unzahl pngs nach movie.
  Um die in einem Film zu kodieren: ``mpv -o=output.webm
  --ovcopts=b=2000000 -of webm -fs --mf-fps=20 mf://movie/*.png``

Ich verteile diese Programme in diesem Verzeichnis unter CC0_.

.. CC0: http://creativecommons.org/publicdomain/zero/1.0/
