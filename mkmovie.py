"""
Make a video from RKI incidences.

This needs in corona.DATA_DIR:

* Aktuell_Deutschland_SarsCov2_Infektionen.csv

from https://github.com/robert-koch-institut/SARS-CoV-2_Infektionen_in_Deutschland.git
and 

* vg2500_krs.*

from https://gdz.bkg.bund.de/index.php/default/open-data/gebietseinheiten-1-2-500-000-ge2500.html

To make a video out of the images left in movie/, run:

mpv -o=output.webm --ovcopts=b=2000000 -of webm -fs --mf-fps=20 mf://movie/*.png

Distributed under CC-0.
"""

import collections
import pickle
import os

import cartopy.crs as ccrs
import cartopy.io.shapereader as shpreader
import matplotlib
import matplotlib.pyplot as plt
import shapely.geometry as sgeom

import corona


# drop rendered images in here (trailing slash required)
DEST_DIR = "movie/"
os.makedirs(DEST_DIR, exist_ok=True)


class _Interpolatable(tuple):
	"""A tuple of scalars suitable for interpolation.

	We want this for age plotting, where we need to keep both the age
	score and the incidence, and interpolate between both of them.

	This class implements the minimum of what we need in iter_interpolated.
	"""
	def __add__(self, other):
		# other must be another pair
		return _Interpolatable(a+b for a, b in zip(self, other))

	def __sub__(self, other):
		# other must be another pair
		return _Interpolatable(a-b for a, b in zip(self, other))
	
	def __truediv__(self, other):
		# other must be a scalar
		return _Interpolatable(a/other for a in self)

	def __mul__(self, other):
		# other must be a scalar
		return _Interpolatable(a*other for a in self)
	

class Kreis:
	"""a model for the infections of a county including its 7-day incidence.

	To operate, you feed cases using add, then call get_incidence, after
	which a new history item is being fed in.

	*Alternatively*, to have age scores as well, use feed_a_score with
	the age score as returned by corona.iter_counts.  Do not call add
	yourself in that case, and use get_age_score then.
	"""
	def __init__(self, key, name, pop, history_length=7):
		self.key, self.name, self.pop = key, name, pop
		self.inf_history = collections.deque([0], maxlen=history_length)
		self.age_history = collections.deque([0], maxlen=history_length)
		self.ages_for_today = []
	
	def add(self, cases):
		self.inf_history[-1] += cases

	def add_with_age_score(self, age_score, cases):
		if age_score is not None:
			self.ages_for_today.extend([age_score]*cases)
		self.add(cases)
		
	def get_incidence(self):
		try:
			return 1e5*sum(self.inf_history)/self.pop
		finally:
			self.inf_history.append(0)

	def get_age_score(self):
		"""returns a pair of (incidence, age-score) for this Kreis.

		Do *not* call get_incidences in addition.
		"""
		if self.ages_for_today:
			self.age_history.append(
				sum(self.ages_for_today)/len(self.ages_for_today))
			self.ages_for_today = []
		else:
			self.age_history.append(None)

		cur_age_mean = None
		non_zero = [score for score in self.age_history if score]
		if non_zero:
			cur_age_mean = sum(non_zero)/len(non_zero)

		if cur_age_mean is not None:
			return _Interpolatable((self.get_incidence(), cur_age_mean))


class Kreise:
	"""a model for the counties of the FRG with their 7-day incidences.

	To operate, feed all cases for a day using add, then call get_incidence_map
	when all data for a day is in.
	"""
	def __init__(self, history_length=7):
		self.kreise = {}
		for row in corona.get_lkr_meta().values():
			self.kreise[row["id"]] = Kreis(
				row["id"],
				row["name"],
				row["pop"],
				history_length=history_length)

		# RKI has split up Berlin.  Re-unify it using aliasing.
		berlin = Kreis(11000, "Berlin", 0)
		for id, bezirk in self.kreise.items():
			if id//1000==11:
				berlin.pop += bezirk.pop
				self.kreise[id] = berlin

		self.kreise[berlin.key] = berlin

	def add(self, key, cases):
		self.kreise[key].add(cases)

	def add_with_age_score(self, key, cases, age_score):
		self.kreise[key].add_with_age_score(age_score, cases)
	
	def get_incidence_map(self):
		"""returns a mapping from kreis-id to 7-day incidences.
		"""
		return dict((kreis.key, kreis.get_incidence())
			for kreis in set(self.kreise.values()))

	def get_age_score_map(self):
		"""returns a maping from kreis-id to history-length-day 
		(incidence, mean age score).

		Call *in alternative to* get_incidence_map.
		"""
		return dict((kreis.key, kreis.get_age_score())
			for kreis in set(self.kreise.values()))
	

# a helper for Proj4Projection
_GLOBE_PARAMS = {'datum': 'datum',
								 'ellps': 'ellipse',
								 'a': 'semimajor_axis',
								 'b': 'semiminor_axis',
								 'f': 'flattening',
								 'rf': 'inverse_flattening',
								 'towgs84': 'towgs84',
								 'nadgrids': 'nadgrids'}


class Proj4Projection(ccrs.Projection):
	"""A projection described by proj4 parameters.

	This is lifted and hacked from cartopy; cartopy's version is glued
	into a query to the epsg service, which we don't want here;
	I've dumbed it down to claiming it works for the whole globe.
	"""
	def __init__(self, proj4_str):
		terms = [term.strip('+').split('=') for term in proj4_str.split(' ')]
		globe_terms = filter(lambda term: term[0] in _GLOBE_PARAMS, terms)
		globe = ccrs.Globe(**{_GLOBE_PARAMS[name]: value 
			for name, value in globe_terms})
		other_terms = []
		for term in terms:
			if term[0] not in _GLOBE_PARAMS:
				if len(term) == 1:
					other_terms.append([term[0], None])
				else:
					other_terms.append(term)
		super(Proj4Projection, self).__init__(other_terms, globe)
		self.bounds = [-180, 180, -90, 90]

	@property
	def boundary(self):
		x0, x1, y0, y1 = self.bounds
		return sgeom.LineString(
			[(x0, y0), (x0, y1), (x1, y1), (x1, y0),(x0, y0)])

	@property
	def x_limits(self):
		x0, x1, y0, y1 = self.bounds
		return (x0, x1)

	@property
	def y_limits(self):
		x0, x1, y0, y1 = self.bounds
		return (y0, y1)

	@property
	def threshold(self):
		x0, x1, y0, y1 = self.bounds
		return min(x1 - x0, y1 - y0) / 100.


class Plotter:
	"""a class for producing the plots.

	In derived classes, you have to at least define a _make_styler(mapping)
	function returing a styler function based on a map from kreis-ids to
	the scalars.

	You will usually want to define a _furnish(mapping, axes) method to add 
	color bars and the like to matplotlib axes.

	Both methods receive the current scalar mapping.

	Feed it by passing in a date and the scalar mapping to plot_one.
	"""
	def __init__(self, dest_dir):
		self.dest_dir = dest_dir
		self.src_crs = Proj4Projection("+proj=utm +zone=32 +ellps=GRS80"
			" +towgs84=0,0,0,0,0,0,0 +units=m +no_defs")
		self.kreis_recs = list(
			shpreader.Reader(corona.DATA_DIR+"vg2500_krs.shp")
			.records())
		self.my_crs = ccrs.EuroPP()
		self.plot_extent = [6, 15, 47, 56]

		for rec in self.kreis_recs:
			rec.geometry.ars = int(rec.attributes["ARS"])
		self.geometries = [r.geometry for r in self.kreis_recs]

		self.ars_to_name = dict(
			(int(rec.attributes["ARS"]), rec.attributes["GEN"])
			for rec in self.kreis_recs)

		self.colormap = matplotlib.cm.get_cmap("cool")

	def _furnish(self, mapping, ax):
		"""A hook called once per plot to draw stuff in addition
		to the map itself.
		"""

	def _make_styler(self, mapping, step):
		"""needs to return a callable(geometry) returning a style dict for
		it.

		You'll find the kreis-id as geometry's ars attribute.
		"""
		raise NotImplementedError("You need to override _make_styler")

	def plot_one(self, date, step, mapping):
		fig = plt.figure()
		ax = fig.add_axes([0, 0, 1, 1], projection=self.my_crs,
			frameon=False)
		ax.patch.set_visible(False)
		ax.set_extent(self.plot_extent, ccrs.Geodetic())
		plt.title(date, loc="left", y=0.9)

		ax.add_geometries(
			self.geometries, 
			self.src_crs, 
			styler=self._make_styler(mapping, step))
		self._furnish(mapping, ax)

		if step is None:
			plt.show()
		else:
			plt.savefig(os.path.join(self.dest_dir, f"{date}-{step:02d}.png"))
			plt.close()


class IncidencePlotter(Plotter):
	"""A class encapsulating drawing plots of incidences.
	"""
	def __init__(self, dest_dir):
		Plotter.__init__(self, dest_dir)
		self.min_vmax = 200
		self.cur_vmax = self.min_vmax

	def _get_top_10(self, incidences):
		"""returns the 10 (incidence, labl) pairs with the highest incidences.
		"""
		return list(sorted(
				((incidence, self.ars_to_name[id]) 
					for id, incidence in incidences.items()),
				reverse=True))[:10]

	def _furnish(self, incidences, ax):
		"""adds a color bar an the top 10 counties to the plot
		"""
		for rank, (incidence, name) in enumerate(self._get_top_10(incidences)):
			plt.figtext(0.02, 0.76-rank*0.05, f"{name} ({incidence:.0f})")

		sm = matplotlib.cm.ScalarMappable(
				self.normalize,
				self.colormap)
		sm.set_array([])
		plt.colorbar(sm, 
			ax=ax,
			shrink=0.8)

	def _make_styler(self, incidences, step):
		if step==0 or step is None:
			top_10 = self._get_top_10(incidences)
			new_max = int(sum(v for v, l in top_10)/len(top_10))
			if new_max!=self.cur_vmax:
				self.cur_vmax = max(self.min_vmax, 
					int(self.cur_vmax+(new_max-self.cur_vmax)/15))
			self.normalize = matplotlib.colors.Normalize(vmin=0, vmax=self.cur_vmax)
		
		def styler(geo):
			return {"facecolor": self.colormap(
				self.normalize(
					incidences[geo.ars]))}
		return styler

	def opening(self, dest_name):
		fig = plt.figure()
		ax = fig.add_axes([0, 0, 1, 1])
		ax.set_xlim(0, 80)
		ax.set_ylim(0, 25)

		ax.text(40, 17, "Corona-Inzidenz in der BRD", fontsize="xx-large",
			weight="bold", horizontalalignment="center")
		ax.text(1, 9, "Mehr Infos: https://blog.tfiu.de/corona-als-film.html")
		ax.text(1, 7, "Infektionsdaten: RKI")
		ax.text(1, 6, "https://github.com/robert-koch-institut/")
		ax.text(1, 4, "Kreis-Polygone: © GeoBasis-DE / BKG 2021")
		ax.text(1, 3, "https://gdz.bkg.bund.de/ VG2500")
		ax.text(1, 1, "Film: Unter CC-0 weiternutzbar.")

		if dest_name is None:
			plt.show()
		else:
			plt.savefig(os.path.join(self.dest_dir, dest_name))


class AgeScorePlotter(Plotter):
	"""A class encapsulating drawing plots of age scores.
	"""
	def __init__(self, dest_dir):
		Plotter.__init__(self, dest_dir)
		self.lower_bracket, self.upper_bracket = 10, 75
		self.normalize = matplotlib.colors.Normalize(
			vmin=self.lower_bracket, vmax=self.upper_bracket)
		self.colormap = matplotlib.cm.get_cmap("rainbow")

	def _get_extremes(self, age_scores):
		"""returns the 5 "oldest" and "younges" kreise in age_scores.

		We discard anything based on incidences below 50.
		"""
		by_age_score = [(age_score[1], self.ars_to_name[id])
			for id, age_score in age_scores.items()
			if age_score[0]>50]
		by_age_score.sort(reverse=True)
		med = len(by_age_score)//2
		top, bottom = by_age_score[:med], by_age_score[med:]
		return top[:5], bottom[-5:]

	def _furnish(self, age_scores, ax):
		top, bottom = self._get_extremes(age_scores)

		for rank, (age_score, name) in enumerate(top):
			plt.figtext(0.02, 0.95-rank*0.05, f"{name} ({age_score:.0f})")

		plt.figtext(0.02, 0.52, "(Die Zahlen sind nicht")
		plt.figtext(0.02, 0.48, "wirklich Altersschnitte!)")

		for rank, (age_score, name) in enumerate(reversed(bottom)):
			plt.figtext(0.02, 0.05+rank*0.05, f"{name} ({age_score:.0f})")

		plt.colorbar(matplotlib.cm.ScalarMappable(
				self.normalize,
				self.colormap), 
			ax=ax,
			shrink=0.8,
			ticks=matplotlib.ticker.FixedLocator(
				[self.lower_bracket, self.upper_bracket]),
			format=matplotlib.ticker.FixedFormatter(["jung", "alt"]))

	def _make_styler(self, age_scores, step):
		def styler(geo):
			# The values in our map are pairs of incidence and age score.
			# Use the incidence as opacity.
			if age_scores.get(geo.ars):
				incidence, age_score = age_scores[geo.ars]
				basecol = self.colormap(
					self.normalize(age_score))
				
				return {"facecolor": basecol[:3]+(min(1, 0.1+0.9*incidence/100),)}
			else:
				return {"facecolor": "white", "edgecolor": (0.9, 0.9, 0.9),
					"linewidth": 0.4}
		return styler

	def opening(self, dest_name):
		fig = plt.figure()
		ax = fig.add_axes([0, 0, 1, 1])
		ax.set_xlim(0, 80)
		ax.set_ylim(0, 25)

		ax.text(40, 17, "Alters-Score für Corona-Infektionen", fontsize="xx-large",
			weight="bold", horizontalalignment="center")
		ax.text(1, 10, "Unter 100er-Inzidenz ist die Farbe entsättigt.")
		ax.text(1, 9, "Mehr Infos: https://blog.tfiu.de/corona-film-teil-2.html")
		ax.text(1, 7, "Infektionsdaten: RKI")
		ax.text(1, 6, "https://github.com/robert-koch-institut/")
		ax.text(1, 4, "Kreis-Polygone: © GeoBasis-DE / BKG 2021")
		ax.text(1, 3, "https://gdz.bkg.bund.de/ VG2500")
		ax.text(1, 1, "Film: Unter CC-0 weiternutzbar.")

		if dest_name is None:
			plt.show()
		else:
			plt.savefig(os.path.join(self.dest_dir, dest_name))


def _iter_maps(row_iterator, adder, get_kreis_map, history_length=7):
	"""yields pairs of (date, scalar-map), where scalar-map maps
	kreis-ids to what get_value returns.

	row_iterator is a function receiving an open file (the RKI CSV) and 
	yielding a data tuple (we don't care what it is, as long as adder
	is fine with it, and row[0] is the date)

	adder is a function receving a Kreise instance and whatever the
	row_iterator returns.

	get_kreis_map is a function receiving our Kreise instance and returning
	the map to plot.
	"""
	kreise = Kreise(history_length)
	cur_date = None

	with open(corona.DATA_DIR
			+"Aktuell_Deutschland_SarsCov2_Infektionen.csv") as f:
		all_rows = sorted(row_iterator(f), key=lambda r: r[0])

	for row in all_rows:
		date = row[0]
		if date!=cur_date:
			if cur_date!=None:
				yield cur_date, get_kreis_map(kreise)
			cur_date = date
		adder(kreise, row)

	if cur_date:
		yield cur_date, get_kreis_map(kreise)


def iter_freqs():
	yield from _iter_maps(
		lambda f: corona.iter_counts(f, True),
		lambda kreise, row: kreise.add(row[1], row[2]),
		lambda kreise: kreise.get_incidence_map())


def iter_age_scores():
	yield from _iter_maps(
		lambda f: corona.iter_counts(f, True),
		lambda kreise, row: kreise.add_with_age_score(row[1], row[2], row[3]),
		lambda kreise: kreise.get_age_score_map(),
		14)


def iter_interpolated(steps_per_day, gen):
	"""yields date, interpolation_index, map triples, interpolating
	between the values in map dicts coming from the generator gen.
	"""
	cur_date, cur_freqs = next(gen)
	for next_date, next_freqs in gen:
		for step in range(steps_per_day):
			yield cur_date, step, dict(
				(key, cur_freqs[key]
					+(next_freqs[key]-cur_freqs[key])/steps_per_day*step)
				for key in cur_freqs
					if cur_freqs[key] is not None and next_freqs[key] is not None)
		cur_date, cur_freqs = next_date, next_freqs
	yield cur_date, 0, cur_freqs


def parse_command_line():
	import argparse
	parser = argparse.ArgumentParser(
		description="Make a movie from RKI data")
	parser.add_argument("-d", "--design_mode", action="store_true",
		help="just render a single frame from a dict left in a previous"
		" run.")
	parser.add_argument("-i", "--interpolate", action="store", type=int,
		dest="n_interpolate", metavar="N", default=7,
		help="interpolate N frames for one day",
		choices=range(1, 20))
	parser.add_argument("-m", "--min-date", action="store", type=str,
		dest="min_date", metavar="ISODATE", default=None,
		help="discard all records earlier than ISODATE")
	parser.add_argument("plot_setup", type=str, help="select what"
		" kind of movie should be made", 
		choices=["inc", "age"])
	return parser.parse_args()


def main():
	args = parse_command_line()
	sample_name = "design.pickle"

	if args.plot_setup=="inc":
		plotterClass = IncidencePlotter
		map_iterator = iter_freqs()
	elif args.plot_setup=="age":
		plotterClass = AgeScorePlotter
		map_iterator = iter_age_scores()
	else:
		assert False

	plotter = plotterClass(DEST_DIR)
	opening_frame_name = "0000-opening-{:03d}.png"
	if hasattr(plotter, "opening"
			) and not os.path.exists(DEST_DIR+opening_frame_name.format(0)):
		plotter.opening(opening_frame_name.format(0))
		for i in range(1, 75):
			os.link(DEST_DIR+opening_frame_name.format(0),
				DEST_DIR+opening_frame_name.format(i))

	if args.design_mode:
		with open(sample_name, "rb") as f:
			plotter.plot_one("2020-01-01", None, pickle.load(f))
		return

	for frameno, (date, step, freq) in enumerate(
			iter_interpolated(args.n_interpolate, map_iterator)):
		# There's some foulup with Eisenach, which is 16056 in my shapes
		# but I think 16063 in RKI data (based on the XLS they link to).
		# Based on that guess:
		if 16063 in freq:
			freq[16056] = freq[16063]
		if args.min_date and date<args.min_date:
			continue
		
		plotter.plot_one(date, step, freq)


if __name__=="__main__":
	main()
