"""
Some minor helpers to deal with RKI Corona data.

Distributed unter CC-0.
"""

import csv
import datetime
import os
import sys


DATA_DIR = os.environ.get("DATA_DIR", "data/")

LANDKREIS = 0
ALTERSGRUPPE = 1
MELDEDATUM = 3
REFDATUM = 4
NEUER_FALL = 6
ANZAHL_FALL = 9


_ALTERSGRUPPE_TO_BIN = {
	"A00-A04": 2,
	"A05-A14": 10,
	"A15-A34": 20,
	"A35-A59": 47,
	"A60-A79": 70,
	"A80+": 85,
	"unbekannt": None,
}

def iter_counts(f, use_ref):
	"""iterates over tuples of (refdate, kreis-id, count, age-bin) from
	the RKI dataset f.

	The date is an ISO date string, kreis-id and count are ints.

	age-bin is an indicator for the age of the patient.  That's an integer,
	2 - 0-4, 10 - 5-14, 20 - 15-34, 47 - 35-59, 70 - 60-79, 85 - 80 and older.

	If use_ref, the date is the RKI's reference date, else the
	notification date.
	"""
	if use_ref:
		date_index = REFDATUM
	else:
		date_index = MELDEDATUM

	row_iter = csv.reader(f)
	# skip the header
	next(row_iter)

	for row in row_iter:
		kind = row[NEUER_FALL]
		if kind!="-1":
			yield (row[date_index], int(row[LANDKREIS]), 
				int(row[ANZAHL_FALL]), _ALTERSGRUPPE_TO_BIN[row[ALTERSGRUPPE]])

def iter_current_recs(f, n_days):
	"""yields (date, id, count, age-score) from the RKI csv in f of the last
	n_days.

	This uses "Meldedatum" rather than "Referenzdatum" for the purposes
	of establishing the record date (mainly because "Referenzdatum" lags
	behind quite a bit).
	"""
	collect_start = (datetime.date.today()-datetime.timedelta(days=n_days)
		).isoformat()
	sys.stderr.write(f"Collecting from {collect_start} on.\n")

	for rec in iter_counts(f, False):
		if rec[0]>=collect_start:
			yield rec


def get_current_counts(f, n_days=7):
	"""returns a mapping for kreis-id to the infection count for the
	n_days days before today.
	"""
	counts = {}
	for _, id, count, _ in iter_current_recs(f, n_days):
		counts[id] = counts.get(id, 0)+count
	return counts


def get_current_age_score(f, n_days=7):
	"""returns a mapping for kreis-id to an age score for n_days before today.
	"""
	a_bins = {}
	for _, id, count, a_bin in iter_current_recs(f, n_days):
		if a_bin is not None:
			a_bins.setdefault(id, []).extend([a_bin]*count)

	return dict((id, sum(vals)/len(vals) if vals else None)
		for id, vals in a_bins.items())


def get_lkr_meta():
	lkr_meta = {}
	with open(DATA_DIR+
			"2020-06-30_Deutschland_Landkreise_GeoDemo.csv") as f:
		for row in csv.DictReader(f):
			row["id"] = int(row["IdLandkreis"])
			row["name"] = row["Gemeindename"]
			row["pop"] = float(row["EW_insgesamt"])
			lkr_meta[row["id"]] = row
	
	return lkr_meta
